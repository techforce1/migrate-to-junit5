# So, you want to migrate your codebase to JUnit5?

That's great\! There's a lot of boring stuff to do though, so I thought
it'd be a good idea to automate some of it. This is done through an AWK
script that uses some features only available in GNU AWK, so there's a
docker image to make sure you can easily run this.

# How do I use this?

Check out my blog post that describes how to use this solution: [Migrate your Java codebase to JUnit5 (almost) automatically](https://techforce1.nl/migrate-your-java-codebase-to-junit5-almost-automatically).
