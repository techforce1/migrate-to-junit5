FROM alpine

RUN apk --no-cache add \
        gawk \
        git \
    && mkdir /src

COPY ["./junit5.awk", "./entrypoint.sh", "/"]

ENTRYPOINT ["/entrypoint.sh"]
